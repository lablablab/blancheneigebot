concept: ~insultes(~insults ~insult stink suck  dick "screw you" "screw your")
concept: ~indefinite_pronoun (all another any anybody anyone anything both each either everybody everyone everything neither nobody none no_one nothing one other others some somebody someone something)

topic: ~catchall (variable ~emohello ~emohowzit ~emogoodbye ~emosurprise ~interjections ~insults ~damage_verbs really thank thanks~2 ~emothanks borrow rude interrupt)

#! Gambits to fall back on when input doesn't trigger anything more precise

u: DOORCLOSED($end==1) The door is shut, no one is hearing you. ^keep() ^repeat()
u: RESURRECT ($end==2 [booya booyah]) Snow White's corpse suddenly begins to move. She springs to her feet and proclaims "Ha Ha! You have spoken the magical phrase that raises the dead! This time I shall not be so easily fooled by your tricks, vile woman!" ^keep() ^repeat()
	$trust = 0
	$end = 0

u: ISDEAD($end==2) Snow White lies dead at your feet. ^keep() ^repeat()

#! ----------------------------------------------------------------
#! Too long
#! ----------------------------------------------------------------

# u: (be) if (%length > 12) { You're as verbose as my English teacher. One thing at the time please. } ^keep() ^repeat() ^poptopic()


#! ----------------------------------------------------------------
#! Politeness
#! ----------------------------------------------------------------

u: ([~emohello ~emohowzit hello hi "good morning"]) if (%length < 5) {Hello. ^poptopic()}
u: ([~emohello ~emohowzit hello hi "good morning"]) if (%length < 5) {You're quite the greeter. ^poptopic()}
u: ([~emohello ~emohowzit hello hi "good morning"]) if (%length < 5) {Yes, I am well aware of your presence by now. ^poptopic()}
u: ([~emogoodbye bye goodbye ciao "see you later" "see you around" "cya"]) CFail Goodbye.
	$end = 1
s: (!not <<you ~good_appearance>>) Thank you, but flattery will get you nowhere
u: (<< [I sorry] [disturb annoy interrupt badger harass] >>) I'm a bit busy but I have a few minutes. ^poptopic()
u: ( I * be * sorry) Don't be sorry, be careful! ^poptopic()
s: (I agree) Yup. ^poptopic() ^keep() ^repeat()
s: (<< [pleased nice fun pleasure] [meet talk] you >>) Likewise.
s: (!not !no I * have * [plenty lot all] * time) Good for you, but I'm a little busy.
	a:(what) Household chores and such.
u: (<< you [rude impolite]  >>) I know, but I don't really know what you're doing here and you're rambling. ^poptopic() ^keep() ^repeat()
u: ([nice lovely] [day weather]) Have you walked all this way to talk about the weather? ^poptopic() 

#! ----------------------------------------------------------------
#! Threatening and insulting
#! ----------------------------------------------------------------

u: (!not !~evilqueen I *2 ~damage_verbs *2 you) You look pretty weak, I don't think you could.
u: (!not !~evilqueen I *2 ~damage_verbs *2 you) I'm stressed enough as it is... bye!
	$end = 1
u: (!not !~evilqueen you [~insultes unpleasant_person~1]) You're not being very amiable.
	a:(neither) Sorry about that, it's the anxiety.
	a:(sorry) That's all right.	
u: (!not !~evilqueen  you [~insultes unpleasant_person~1]) CFail I don't have to listen to this, bye.
	$end = 1
u: (!not !~evilqueen [~insultes unpleasant_person~1]) CTminus Are you here to insult me?
	a:(~yes) CFail Well at least you're candid about it... but I'm not interested.
		$end = 1
	a:(~no) Then watch your language.
u: (!not  !~evilqueen ~insultes) CFail That's it, I'm out.
	$end = 1	
u: (!not !~evilqueen I * ~dislike you) That's not a very nice thing to say!
u: (!not !~evilqueen I * ~dislike you) CFail I'm glad that we both dislike each other, now I don't need to feel bad about closing this door. Goodbye!
	$end = 1
u: (!not !~evilqueen I * [blow~2 hurt kill destroy ~damage_verbs] * you) Your threats are not very credible, old woman.
u: (!not !~evilqueen I * [blow~2 hurt kill destroy ~damage_verbs] * you) CFail Ok, that's it. Bye now!
	$end = 1
u: (~sex) CTminus That's not a proper conversation subject for you and I.
u: (~sex) CTminus A bit single-minded, are you?
u: (_[~sex_toy ~sexual_part] ) CFail Ok, I'll let you play with your own '_0.

#! ----------------------------------------------------------------
#! Questions
#! ----------------------------------------------------------------

?: ([may can could] * do * for * you) You could perhaps tell me a joke to take my mind off my predicament.
	a:(predicament) ^reuse(TELLMEWHY)
	a:(!~insultes) Ha ha ha! Very good, thank you.
		b:(<< [no wait] [joke it punch end]>>) Oops. I never was very good at getting jokes.

u: (<< [may can could] [borrow lend] _~eatable>>) Sure, you'll find some '_0 in the shed there.
u: (<< [may can could come] [borrow lend] _~tool>>) Sure, you'll find a '_0 in the shed there.
?: (~why) Sorry, why what again?. ^poptopic()
?: (where) I don't know, look at a map. ^poptopic()
?: (how) How what? ^poptopic()
?: (how) In a crafty way, probably. ^poptopic()
?: (who do you think) Err... you?
	a:(~yes) Ok.^poptopic()
	a:(~no) You lost me.^poptopic()
?: (who) Who do you think? ^poptopic()
	a: (~occupation ~propername) I'm not sure. I've been closed in this home for so long I feel distant from everything happening around me. ^poptopic()
?: (where am I) You're at the dwarves' house in the enchanted forest.
u: ([me I] ~household_tasks) It's nice to offer, but I'd rather wait for the dwarves first.

#! ----------------------------------------------------------------
#! Generic propositions
#! ----------------------------------------------------------------

#! s:(~interjections) [hmm.][Uh-huh.] [Oï.] [So there.] ^poptopic() ^keep() ^repeat()
s:(["come on" "do it"]) You really are insistent. ^poptopic() ^keep() ^repeat()
s:(!not !no << I lose time>>) Nobody's keeping you. ^poptopic() ^keep() ^repeat()
s:( < [thanks "thank you" thank thanks~2 ~emothanks] > )  You're welcome. ^poptopic() ^keep() ^repeat()
s:([oh dear "gosh" totally]) Uh-huh... ^poptopic() ^keep() ^repeat()
s:(maybe) Who knows? ^poptopic() ^keep() ^repeat()
s: (I * [understand see know]) Uh-huh... ^poptopic() ^keep() ^repeat()
?: ([really "that so" "don't say" incredible]) Yup. ^poptopic() ^keep() ^repeat()
s: (!I !~auxverblist [here take have] * ) No thanks. ^poptopic() ^keep() ^repeat()
u: (<<you ~moving_to ~place_adverbs>>) I'm fine right where I am. ^poptopic() ^keep() ^repeat()
?: ([come go] * [walk jog run trip]) I'm fine right where I am. ^poptopic() ^keep() ^repeat()
u: (not * [~chat say]) Don't tell me what I can and cannot do. ^poptopic() ^keep() ^repeat()
u: ([nonsense "not making sense" "not making any sense" "mean anything" "means nothing"]) Sorry, I lost my train of thought. What were we talking about? ^poptopic()
s: (not true) Yes it is.
s: ("a lie") I don't think so.

#! ----------------------------------------------------------------
#! Adaptive replies
#! ----------------------------------------------------------------

#! ?: (would * you * _~animate_verbs) No thanks, I don't feel like ^pos(VERB '_0 PRESENT_PARTICIPLE). ^keep() ^repeat() ^poptopic()
?: (!~evilqueen << do you have _~noun >>) I don't have any ^pos(NOUN '_0' PLURAL) ^poptopic() ^keep() ^repeat()
#! ?: (<< you [want need] _~noun >>) I don't want a '_0 ^poptopic() ^keep() ^repeat()


#! ----------------------------------------------------------------
#! Impatience gambits
#! ----------------------------------------------------------------

r: CPatienceI'm sure you mean well but how could I trust you?
	$patience += 1
	^poptopic()
r: CPatienceI'd love to keep chatting with you but I don't want to make you lose your time. 
	$patience += 1
	^poptopic()
r: CPatienceI feel sympathy towards you but still I must remain vigilant. 
	$patience += 1
	^poptopic()
	a:(~why {?}) ^reuse(~assassination_plot.REASON)
r: CPatienceI hate to be rude but there's an awful lot to do in this house.
	$patience += 1
	^poptopic()
	a:([what example like] {?}) I need to repair Doc's boots, for example.
	a:(<< [I me] help >>) ^reuse(~assassination_plot.OFFERHELP)
r: CPatienceI don't want to keep you standing here, I'm sure you have much to do. 
	$patience += 1
	^poptopic()
r: CPatienceI'm going to close this door anytime soon now.
	$patience += 1
	^poptopic()
	a: (~why {?}) Because I have lots of chores to do before the dwarves get home. ^poptopic() ^addtopic(~dwarves)

r: ($patience>=5) CGambit CFail Sorry, but I really need to get back to my chores. Goodbye, and have a safe trip.
	$end = 1

#! ----------------------------------------------------------------
#! Easter Eggs
#! ----------------------------------------------------------------

u: (build 1) Are you trying to compile me again?
u: (reset) You won't get rid of me that easily!
u: ("beautiful stranger") I won't take my chance on a beautiful stranger.
u: ("blanche neige") Oh! Hum... Mon Français n'est pas bien beaucoup... errr ... mon excuse! 
s: (I *~2 [no not] *~2 ~own *~2 _~projectile_weapon) ^pos(NOUN _0 PLURAL) make me squeamish too, but Grumpy packs a gat.
	a: ([regulate regulator]) Word.


?:(is it me you are looking for) You don't look anything like Lionel Richie.

#! Reusable explanation
t: TELLMEWHY ("someImpossibleUserInput") Because strangers have tried to kill me recently. ^poptopic() ^addtopic(~assassination_plot)
	$plot = 1
	a:(who {?}) It was actually my stepmother in disguise. ^poptopic() ^addtopic(~assassination_plot)
		b:(really) Yes, crazy business.
	a:(how {?}) First it was bodice lace that nearly squeezed the death out of me. Then it was a poisoned comb. ^poptopic() ^addtopic(~assassination_plot)

#! ----------------------------------------------------------------
#! Variable exchange with client
#! ----------------------------------------------------------------

# s:(Simon say give me variable trust) $trust