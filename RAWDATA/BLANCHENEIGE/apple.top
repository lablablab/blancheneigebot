
concept: ~delicious (delicious delectable savory yummy tasty fresh)
concept: ~samaritan (samaritan do-gooder humanitarian "johnny on the spot" helpfulness~2)
concept: ~takethis ("take this" "would you like" "how about" "would you care for" "would you care to join me")

topic: ~sell_apple (apple ~fruit buy sell food hunger hungry snack meal give ~delicious it eat gift present ~eatable)

#! Possible related pronouns
u: (_it) ^mark(apple _0)    ^repeat() ^keep()
u: (_them) ^mark(apple _0)  ^repeat() ^keep() 
u: (_they) ^mark(apple _0)  ^repeat() ^keep() 
u: (_one) ^mark(apple _0)  ^repeat() ^keep() 
u: (_fruit)  ^mark(apple _0)  ^repeat() ^keep() 
u: (_bite) ^mark(apple _0)  ^repeat() ^keep() 

 
u: ($end==1) ^reuse(~catchall.DOORCLOSED)
u: ($end==2 ["booya" "booyah"]) ^reuse(~catchall.RESURRECT)
u: ($end==2) ^reuse(~catchall.ISDEAD)

#! ----------------------------------------------------------------
#! "I will have one too tactic"
#! ----------------------------------------------------------------

s:(!you !not I * [will could] * [eat taste try bite] * apple) CTplus That is somewhat convincing but you could have poisoned only parts of it!
	$trust += 1
	a:(~why {?}) That way you could you could take a bite out of the part that wasn't poisoned to trick me into eating the rest.

?:(!you !not <<[what perhaps could] I [eat taste try bite] apple>>) CTplus That is somewhat convincing but you could have poisoned only parts of it!
	$trust += 1
	a:(~why {?}) That way you could you could take a bite out of the part that wasn't poisoned to trick me into eating the rest.

u:(<< I eat apple whole >>) That's easy: you eat the good apple and give me the poisoned one.

u:(<<[share split half] apple>>) CTplus That helps with my trust issues, but you could have poisoned only half of the apple.
	$trust += 1
	a: (~why {?}) So that I might eat the poisoned half!
		b: (you * ~choose * [half part piece chunk]) Sorry, but I'm not sure that I'm willing to play Russian roulette for an apple.
	a: (you * ~choose * [half part piece chunk]) Sorry, but I'm not sure that I'm willing to play Russian roulette for an apple.



#! ----------------------------------------------------------------
#! Bribe tactic
#! ----------------------------------------------------------------

u:(!earn !make [I here take have give] * [~currency dollars gold silver coins money cash]) CTminus You're giving me money to taste your apple? This is getting really suspicious.
	$trust -= 1

?:(<<help [for against pay] apple>>) I'm not looking for a reward, it's just the circumstances. ^poptopic() ^addtopic(~assassination_plot)

#! ----------------------------------------------------------------
#! Threatening
#! ----------------------------------------------------------------

u: (<<[~acquire_imperatives here taste buy want~1 ~acquiring_verbs have] [~insults unpleasant_person~1] apple>>) You're not being very amiable.
u: (<<[~acquire_imperatives here taste buy want~1 ~acquiring_verbs have] [~insults unpleasant_person~1] apple>>) CFail I don't have to listen to this, bye.
	$end = 1

#! ----------------------------------------------------------------
#! Selling the apple
#! ----------------------------------------------------------------

u: (!not !no <<you be [hungry famished]>>) That's fine, I just ate a raw turnip.

u: (<< [eat apple fruit] [help make] you feel better >>) Maybe... but a poisoned apple certainly wouldn't help.

u: (<<!no !not apple _[~delicious tasty "sweet"]>>) CTplus It does look '_0.
	$trust += 1
	a:(!no !not _0?) Yes, yes, unbelievably _0


u: (<<!no !not apple [cheap bargain "low price" "good price" free gratis]>>) Price is not the issue here.
	a:(what {?}) Trust is the issue.

u: (<<!no !not apple [magical~1 invisible powerful magic enchanted miraculous]>>)
	I've become wary of magic.
	a:(~why {?}) My stepmother has been using magic to kill me.
		$plot = 1
		^poptopic() ^addtopic(~assassination_plot)

u: (<<apple not [poisoned poison]>>) I may be naive, but I'll need more than your word.

u: (!not <<apple [poison poisoned]>>) CTminus I appreciate your candour but I don't think I'll be eating this apple now.
	$trust -= 1

u: (<<!no !not apple [wellness~1 healthy good "good for you" nutritive "well-being" "fat" organic] >>) CTplus It's true that apples are healthy.
	$trust += 1

u: (<<!no !not apple day doctor>>) I knew you'd say that.

u: (!no !not dwarf~1 * like~5 * apple) They do, but they're very picky when it comes to the origin of what they eat.
	$dwarves = 1

u: (<<!no !not  you apple pie >>) The dwarves would love this but they won't eat anything they haven't grown themselves.
	$dwarves = 1

u: (<<!no !not  apple you [plain ugly "less beautiful" hideous "bad looking"] >>) That's interesting, but I'm not very fond of magic.
	$trust += 1
	a:(~why {?}) CTplus My stepmother has tried using magic to kill me.
		$plot = 1
		^poptopic() ^addtopic(~assassination_plot)

u: (<<!no !not apple make [feel life] better >>) CTplus It's true that fruits do that.
	$trust += 1

#! ----------------------------------------------------------------
#! Apple is Magical (in a good way)
#! ----------------------------------------------------------------

s: (<<!no !not apple [protect defend help]>>) Wait, this apple can protect me from my stepmother?
	a:([~Yes protect defend help maybe perhaps] ) CTplus I'd like to believe that.
		$trust += 1
	a:(~No) Too bad.

s:(<<!no !not apple [protect defend help]>>) Yes, you've said that. It's a tempting thought but you could be lying.

s: ($stepmother!=1 <<apple _["peace offering" truce]>>) I don't need a '_0, I need to be left alone.

u: ( [black white good bad] magic) I equally discriminate against all type of magic.

#! ----------------------------------------------------------------
#! Fishing for info
#! ----------------------------------------------------------------

?: (~why * [accept~5 ~taste try~1 ~eat]) The last two times a strange woman came to my door to give me something, it turned out to be assassination attempts from my stepmother. 
	$plot = 1

?: (!no !not << you like eating apple >> ) Of course.	

?: (<<!no !not  you like _~eatable >>) Sure, everyone likes '_0.

u: (<< ~why [I one someone somebody] poison [apple food] >>) Because you could be my stepmother who wants to kill me disguised as an innocent old woman.



#! ----------------------------------------------------------------
#! Offer apple
#! ----------------------------------------------------------------

s:(<< [gift present] you >>) You're offering me that apple?
	a:(~yes) That's sweet. However I can't accept anything from strangers.
		$offered = 1
		b: (~why {?})^reuse(justify_identity.TELLMEWHY)
	a:(~no) What is it then?
		b: (_~noun) Oh, but I really don't need '_0.

s: (<< leave apple later >>) Keep it for someone who will eat it.	

s: (<< you not should eat apple >>) Thank you for the warning.

s: ( $trust<5 [offer give] you *~2 apple) That's nice of you but I don't accept fruits from strangers.
	a:(~why {?}) ^reuse(justify_identity.TELLMEWHY)

s: ( << I [bring have give] apple you >> ) That's mighty nice nice of you, but I am not especially hungry right now.

#! t: ($trust>=5) CWin All right, I'll have a bite of this apple. Arrrhh you vile woman! I'm too young to die! The dwarves will definitely think me an idiot now! ^addtopic(~catchall)
#! 	$end = 2

u: ($identity!=1 $offered!=1 !they !~dwarves [share ~acquire_imperatives here taste buy want~1 ~acquiring_verbs have] * apple ) 
	$offered = 1
	Excuse me, but who are you to be offering apples to strangers? ^poptopic() ^addtopic(~justify_identity) 
		a: (_*) ^input(I am '_0) ^fail(sentence)  
	
#! u: ($offered!=1 $peddler!=1 apple) Sorry, are you here to sell apples?
#!	a:(~yes) That's nice but I can't accept anything from strangers.
#!		$purpose = 1
#!		$peddler = 1
#!		b:(~why) ^reuse(justify_identity.TELLMEWHY)
#!	a:(!give ~no) Then what are you doing here?
#!	a:(give) That's nice but I really can't accept anything in the circumstances.
#!		b:(~why) ^reuse(justify_identity.TELLMEWHY)

u: OFFER($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple ) No thanks, I don't trust you enough.
u: ($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple ) I still don't trust you enough to eat that.
u: ($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple  ) Not eating that apple.
u: ($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple  ) Stop waving that apple in my face.
u: ($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple  ) There are other people in this world that could want this apple!
u: ($trust<5 !~insults !unpleasant_person~1 [bite ~acquire_imperatives here taste buy want ~acquiring_verbs ~eat] * apple  ) No. ^keep() ^repeat()
u: WIN($trust>=5) CWin All right, I'll have a bite of this apple. Arrrhh you vile woman! I'm too young to die! The dwarves will definitely think me an idiot now! $end = 2 ^poptopic() ^addtopic(~catchall)

s:(I * have * too~1 * apple) There's no such thing as too many apples.

u: (<<!no !not ~takethis ~food>>) You mean one of those apples?	
	a:($trust<=5 ~yes) No thanks, I don't trust you enough.
	a:(~No) In any case, I'm not hungry, thanks.

?: (~why *~2 you *~2 not ~want * apple > ) I don't know you so could be poisoned!

s: (!not apple *~2 be [come from] *~3 tree) Really? I didn't know it was an apple tree.

#! ----------------------------------------------------------------
#! Topic-related gambits
#! ----------------------------------------------------------------

r:($offered==0) CGambit That apple you're holding, is that your lunch? 
	a: (~yes) Lucky you!
		b: ($trust>=5 you [~eat share share~2 piece~4 ~acquire_imperatives]) ^reuse(WIN)
		b: ($trust<=5 you [~eat share share~2 piece~4 ~acquire_imperatives]) Thank you, but I don't really know you well enough to accept.
		$offered = 1
	a: (~no) Is it the reason of your visit?
		b: (%length<=2 ~yes) So what about the apple, then?
		b: (~no) Oh, ok.
	a: (!I !you [lunch gift]) How nice of you.

r:($offered==0 apple) CGambit That's a nice looking apple you have there.
	a: ($trust>=5 you [~eat share share~2 piece~4 ~acquire_imperatives]) ^reuse(WIN)
	a: ($trust<=5 you [~eat share share~2 piece~4 ~acquire_imperatives]) I'd like to but I still don't know you enough.
		$offered = 1
	a: ([thank thanks~2 ~emothanks]) It has been a while since I last saw such a plump fruit... 
	a: (!swallow_food_verbs ~fruit?~eatable) Fruits are good for you.
	a: (I *~4 ~meal) Good thinking.
	

t: ($offered==1) 	[CGambitI'm sorry to be like that but I just can't accept anything from anyone.]
					[CGambitThat apple seems nice, but I wouldn't risk my life for a generic fruit.]
					[CGambitI know I sound impolite but I'm sure you would do the same if you feared to be poisoned.]
	a:([~why "how come" explain "tell me" poisoned]) ^reuse(justify_identity.TELLMEWHY) 